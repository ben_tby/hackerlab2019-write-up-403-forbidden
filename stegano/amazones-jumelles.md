## Challenge: Amazones jumelles | 250pts

> *La puissance des jumelles, c'est qu'elles vont toujours de pair. C'était pareil pour les amazones.*

Là on a deux images jumelles à manipuler pour valider l'épreuve

![Alt text](https://bitbucket.org/ben_tby/hackerlab2019-write-up-403-forbidden/downloads/a.png "Jumelle 1")
![Alt text](https://bitbucket.org/ben_tby/hackerlab2019-write-up-403-forbidden/downloads/aa.png "Jumelle 2")

On entrevoit très vaguement le flag sur la deuxième image mais décidément pas moyen de tirer quoi que ce soit tel quel

On va essayer de combiner les deux images pour voir si on rend lisible le flag

Pour ce faire on utilisera `Adobe-Photoshop-CC`

On superpose les deux images et, tout en gardant l'opacité des deux à 100%, on choisit pour mode de fusion du calque supérieur `Différence` 

![Alt text](https://bitbucket.org/ben_tby/hackerlab2019-write-up-403-forbidden/downloads/method.png "Fusion")

On remarque que le flag est désormais beaucoup plus visible.

Voyons ci-dessous le flag avant et après la fusion


![Alt text](https://bitbucket.org/ben_tby/hackerlab2019-write-up-403-forbidden/downloads/aa.png "Jumelle 2")
![Alt text](https://bitbucket.org/ben_tby/hackerlab2019-write-up-403-forbidden/downloads/AJ-flag.png "Jumelle 2")


# Le Flag est donc `Flag: CTF_XOR_FDFGDGDFGE`