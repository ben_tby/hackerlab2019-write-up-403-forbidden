## Challenge: Photo de famille | 100pts

> *Un souvenir inoubliable!*


Ici on affaire à une image jpeg. L'épreuve étant classée dans la catégorie Steganographie, premier réflexe: essayer d'avoir toutes les infos sur l'image en question. Pour ce faire, nous emploierons le très célèbre steghide. On exécute donc steghide --info  amazones.jpg

La sortie nous indique un certain nombre d'informations basique mais le plus intéressant vient quand le programme nous demande si on veut aller un peu plus loin (voir image ci dessous)

![Alt text](https://bitbucket.org/ben_tby/hackerlab2019-write-up-403-forbidden/downloads/steghide-01.png "infos-image")


On sait désormais qu'il y a un certain flag.txt qui est dissimulé
On l'extrait au moyen de la commande steghide extract  -sf amazones.jpg puis on l'affiche


![Alt text](https://bitbucket.org/ben_tby/hackerlab2019-write-up-403-forbidden/downloads/steghide-02.png "extract-image")



Le Flag est donc `Flag: CTF_YOUROCKTAKEYOURFLAG`