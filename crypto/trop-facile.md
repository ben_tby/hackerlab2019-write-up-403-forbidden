## Challenge: Trop facile | 100pts

> *-.-. - ..-. ..--.- - .-. --- .--. ..-. .- -.-. .. .-.. . -.-. --- -- -- . -.-. .... .- .-.. .-.. . -. --. .*

Ici on voit une succession  de caractères `-` et `.`
Ça fait naturellement penser à l'alphabet morse que voici ci dessous

*International_Morse_Code-fr.svg*

On décode caractère par caractère et on trouve le flag....  Sinon presque. En réalité le `..--.-` n'a aucun équivalent dans l'alphabet présenté ci dessus. Après quelques recherches on découvre que cela équivaut au caractère underscore `_`
Tout étant déchifré on obtient le flag `CTF_TROPFACILECOMMECHALLENGE`

Une autre solution consiste à utiliser le site https://www.dcode.fr/code-morse pour déchiffrer automatiquement le code morse
