# Code de Houégbadja | 100

> Les cyber-amazones connaissaient parfaitement le code de Houégbadja. C'est un code basé sur 41 articles formant la constitution du Danhomey. Pour le déchiffrer les cyber-amazones ramenaient ce code dans une base de 10 articles, souvent bien moins difficile à garder. Pour valider le flag, précède-le de CTF_

En prêtant attention au texte de l'énoncé, nous avions déduit:

* La chaine est encodé en **base 41**
* Précédé de **CTF_** , l'encodage en **base 10** est le resultat attendu.

Avec nos préacquis en système de numération nous aboutissons à cette opération:
```
6*41^3 + 12*41^2 + 21*41^1 +5
```
 Avec notre puissant calculateur python on fait le calcul:
 
 

```
izzy@403forbidden:~/Documents/Hackerlab/Houegbadja$ python3
Python 3.7.5 (default, Oct 27 2019, 15:43:29) 
[GCC 9.2.1 20191022] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> print(6*41**3 + 12*41**2 + 21*41**1 +5)
434564
>>
```


# Resultat attendu: CTF_434564