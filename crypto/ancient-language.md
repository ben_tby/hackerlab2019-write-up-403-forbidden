## Challenge: Ancient language | 300pts

> *C'est un language connu des ancêtres des cyber-amazones.*

Dans ce challenge on a une image contenant une suite de symboles qui une  fois déchiffrés formeront le flag.
Il faudra faire quelques recherches pour trouver qu'il s'agit d'un alphabet ancien inspiré des traces de pattes de dinosaures et qui porte le nom de "dinotopia alphabet" ou encore "The footprint alphabet". En voici les détails 


![Alt text](https://bitbucket.org/ben_tby/hackerlab2019-write-up-403-forbidden/downloads/ALPHABETS-OF-D.2-1024x965.jpg "Alphabet")

On déchiffre caractère par caractère et on obtient `CTF_RESPECT`