## Challenge: Very simple hiding | 100pts

> *C'est Toutefois Faisable. Avec autant De Talents présents A cette compétetion, Nous Sommes sûr Que la Cyber-Armée béninoise sera la Meilleure mondiale.*

Ici le résultat est quelque peu trivial. Le texte qui nous est soumis a la particularité de défier les conventions typographiques relatives à la casse. De plus les premières majuscules rencontrées forment CTF. On devine donc très rapidement que la solution consiste à aligner les majuscules contenues dans le texte. Il faut tout de même faire attention à ne pas insérer de underscore "_" après la chaine "CTF". Le résultat donne donc  `CTFANDTANSQCAM`