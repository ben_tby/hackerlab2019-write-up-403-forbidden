# Challenge: Ingénieux | 150pts
>	*AAABABAABBAABAB ABAABAABAABABABABBBAABAAABAABABAAAABABAAAABAABAABBBABAAAABAABAABAAAAABABBBAABBAB*

Après quelque checking sur internet, nous avons compris qu'il s'agit du chiffre de Bacon.
Avec https://mothereff.in/bacon , on l'a fait.

![](https://bitbucket.org/ben_tby/hackerlab2019-write-up-403-forbidden/downloads/motherff.png)

Resultat attendu: CTF JEVOISQUETUESBON