## Challenge: a^b | 300points

>    4354465f4f6e6542797465586f724973426164617307

On a perdu un bout de temps a essayer de cracker, sans rien avoir de concrêt, avant d'avoir eu l'idée d'essayer de voir le moins intelligent possible. :)

```
izzy@403forbidden:~$ python3
Python 3.7.5 (default, Oct 27 2019, 15:43:29) 
[GCC 9.2.1 20191022] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> bytes.fromhex('4354465f4f6e6542797465586f724973426164617307').decode('utf-8')
'CTF_OneByteXorIsBadas\x07'
>>> 

```
Nous avons essayer CTF_OneByteXorIsBadas en vain. C'est évidemment l'octet endommagé qui manque.
En remplacant **\x07** par **s** nous obtenons CTF_OneByteXorIsBadass

# Flag: CTF_OneByteXorIsBadass