# Challenge: Bad practice | 100pts
> 9ed6c336c58699768ee0ff4782532203a06f541aec022015d8fa4b23813a36d5


Le nom du challenge nous fait penser à un mot de passe, vu encodage, à un *hash de mot de passe*.
Nous l'avons alors soumis à https://crackstation.net/

![](https://bitbucket.org/ben_tby/hackerlab2019-write-up-403-forbidden/downloads/crackStation.png)

# Flag: CTF_rasdzv3