# Challenge CryptoMiner?, 150

>    Trouver le domaine que ce malware essaie de contacter et renseignez-le comme flag. Attention: Ceci est un vrai malware. Traitez-le seulement dans un sandbox. Mot de passe ouverture du zip: miner

Nous avons uploadé l'exe sur https://tria.ge/ . Après analyse, dans le rapport la session **Network** nous donne la réponse.
![](https://bitbucket.org/ben_tby/hackerlab2019-write-up-403-forbidden/downloads/crypto.png)

# Flag: v9.proxxxy.xyz 