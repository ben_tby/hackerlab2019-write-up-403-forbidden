# Hello HackerLab, 300

Le [fichier](https://bitbucket.org/ben_tby/hackerlab2019-write-up-403-forbidden/downloads/hello-hackerlab.crx) du challenge est un .crx. Un fichier CRX est une extension qui ajoute des fonctionnalités ou des thèmes supplémentaires au navigateur Web de Google Chrome.
https://crxextractor.com/ Ce site nous a permis d'avoir le code source du fichier sous forme d'archive .zip .
Dans l'archive, le fichier main.js se présente comme suit:

```javascript

chrome.app.runtime.onLaunched.addListener(function() {
  // Center window on screen.
  var screenWidth = screen.availWidth;
  var screenHeight = screen.availHeight;
  var width = 500;
  var height = 300;
  //console.log("43,54,46,5f,59,6f,75,57,69,6e,54,68,65,43,68,72,6f,6d,65,41,70,70,52,65,76,65,72,73,65,42,65,67,69,6e,6e,65,72,42,61,64,67,65");

  chrome.app.window.create('index.html', {
    id: "helloWorldID",
    outerBounds: {
      width: width,
      height: height,
      left: Math.round((screenWidth-width)/2),
      top: Math.round((screenHeight-height)/2)
    }
  });
});
```

Sans plus tarder, voyons voir ce que ce code hex nous cache.

```shell
izzy@403forbidden:~$ python3
Python 3.7.5 (default, Oct 27 2019, 15:43:29) 
[GCC 9.2.1 20191022] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> a="43,54,46,5f,59,6f,75,57,69,6e,54,68,65,43,68,72,6f,6d,65,41,70,70,52,65,76,65,72,73,65,42,65,67,69,6e,6e,65,72,42,61,64,67,65".replace(",", "")
>>> bytes.fromhex(a).decode('utf-8')
'CTF_YouWinTheChromeAppReverseBeginnerBadge'
>>> 

```

Wesh on a gagné le badge lol.

# Flag: CTF_YouWinTheChromeAppReverseBeginnerBadge