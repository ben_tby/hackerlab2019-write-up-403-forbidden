# Challenge: Friendly Ransomware | 200pts

>	*Il semble que pour augmenter ses revenus, Djakpataglo envisageait de quitter le gaye pour entrer dans le piratage professionel. Les renseignements indiquent qu'il se serait attaché les services des hackers de l'Est, les GankpaBrokers. Ils avaient démarré le développement d'un ransomware qui semble-t-il n'est pas encore très bien finalisé. bjCSIRT a mis la main dessus et demande l'expertise des cyber-amazones pour comprendre son fonctionnement.*

Nous avons utilisé la platforme https://tria.ge afin d'observer en temps réel le comportement de l'exe sur un Win7 et un Win10 et de consulter un rapport. Dans ce rapport, du côté de Win10 la partie **Defense Evasion** a retenu notre attention.

![](https://bitbucket.org/ben_tby/hackerlab2019-write-up-403-forbidden/downloads/report.png)

Nous avons ensuite recherché le hash md5 du fichier sur le plus grand moteur de recherche malware afin d'avoir d'avantage d'information.
Dans **Behavior**, nous avons été consulté les **Registry actions**.

![](https://bitbucket.org/ben_tby/hackerlab2019-write-up-403-forbidden/downloads/Registry_Actions.png)

# Flag: CTF_AlphaReverserAndTheOnlyOmega
