## Challenge: Confiance zéro! | 150pts

> *Les agents du HackerLab ont subit une attaque et font appel aux cyber-amazones. Mais dans le cyberespace béninois, il faut faire attention à ne pas faire confiance à n'importe qui. Les agents du HackerLab ne font confiance qu'aux cyber-amazones venant de l'ANSSI-Benin ou du bjCSIRT. http://hackerlab.bj:8001/web02/web02_ffzpiizef456_ori.php*

En suivant le lien fourni on a le message suivant *"Prouvez votre origine ou retournez chez vous cyber-amazone! "*

Décidément tout parle d'origine par ici
Alors d'une part on a notre origine à prouve et d'autre part on nous dit *"Les agents du HackerLab ne font confiance qu'aux cyber-amazones venant de l'ANSSI-Benin ou du bjCSIRT"*

On se dit qu'on doit prouver qu'on a pour origine tantôt l'ANSSI, tantôt le bjCSIRT.
Comment prouver ça ??

Commençons par analyser la requête qui va vers le serveur web quand on clique sur le lien

Pour ça on utilisera notre proxy préféré `BurpSuite`

Voici ci-dessous la requête

![Alt text](https://bitbucket.org/ben_tby/hackerlab2019-write-up-403-forbidden/downloads/origin.png "Requète")

Comment la modifier pour prouver notre origine ?

Éclair de génie !! On pense qu'il faudra se servir du `HTTP Origin Header`

Alors la syntaxe est simple: `Origin: <value>`

<value> peut être une chaine vide (""), un hostname/adresse web (google.com), ou une quelconque chaine (sample)

Premier réflexe on ajoute `Origin: bjCSIRT` à la requête.... Aucun flag en vue

Seconde tentative on ajoute le hostname du site web du bjCSIRT `Origin: csirt.gouv.bj` à la requête...

Toujours pas de flag en vue mais cette fois ci on remarque de nouveaux éléments très parlants dans la réponse du serveur

![](https://bitbucket.org/ben_tby/hackerlab2019-write-up-403-forbidden/downloads/response-01.png "Réponse du serveur")

On a touché la cible.... Sinon presque.

On a l' `Origin` qu'il faut. Par contre on a pas la bonne méthode  puisque les méthodes autorisées sont `POST, OPTIONS`

On reprend notre requête et on corrige la méthode de `GET` en `POST` et bingo

![](https://bitbucket.org/ben_tby/hackerlab2019-write-up-403-forbidden/downloads/response-02.png "Flag trouvé")


# Le Flag est donc `CTF_GEG533HHGGSGG1CPJM`