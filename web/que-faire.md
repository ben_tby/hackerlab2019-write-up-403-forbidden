## Challenge: Que faire? | 100pts

> *Pour défendre notre cyber-contrée, les cyber-amazones devaient choisir la bonne méthode. http://hackerlab.bj:8001/web01/web01_sqfrs35_quefaire.php*

En suivant le lien fourni on a une page web vierge

On commence à se demander *Que faire?*

L'énoncé nous donne une piste: *"les cyber-amazones devaient choisir la bonne méthode"*

On pense qu'on doit vérifier la méthode HTTP utilisée dans la requète soumise au serveur et trouver la méthode appropriée pour avoir le flag

Commençons par consulter le contenu de la requête.
Pour ça on utilisera une fois de plus le proxy `BurpSuite`

Voici ci-dessous la requête

![Alt text](https://bitbucket.org/ben_tby/hackerlab2019-write-up-403-forbidden/downloads/QF-req.png "Requète")

Trouvons à présent *"la bonne méthode"*
On a plusieurs méthodes possibles telles que `GET`, `HEAD`,  `POST`, `PUT`, `DELETE`, `CONNECT`, `OPTIONS`, `TRACE`, `PATCH`

Place au fuzzing !!

Eurêka !! 
Dès qu'on emploie la méthode `OPTIONS` on obtient le flag en réponse

![Alt text](https://bitbucket.org/ben_tby/hackerlab2019-write-up-403-forbidden/downloads/QF-flag.png "Flag trouvé")

Le Flag est donc `CTF_34GFGLFRFE343432323R2`

En fin de compte pour répondre à la question **Que faire?** il fallait commencer par vérifier quelles **OPTIONS** s'offrent à nous.