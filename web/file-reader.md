## Challenge: File Reader | 300pts

> *Recherche et lis le fichier flag.txt http://51.91.120.156/rumble/*

En suivant le lien fourni on a un petit formulaire qui permet d'entrer le nom d'un fichier à lire

![Alt text](https://bitbucket.org/ben_tby/hackerlab2019-write-up-403-forbidden/downloads/form.png "Formulaire")

Très rapidement on pense à une vulnérabilité LFI à exploiter. De plus le contenu du formulaire est envoyé par la méthode GET donc on peut directement manipuler l'url.Réflexe d'un habitué des vulnérabilités LFI: se rendre dans `/etc/` comme pour lire `/etc/passwd` sauf que dans ce contexte précis on cherche à lire `flag.txt`

Tel que l'indique la capture ci dessus on se trouve dans `/var/www/html/rumble/index.php`

À partir de là on sait comment atteidre `/etc/flag.txt`

En manipulant l'url on se lance sur http://51.91.120.156/rumble/index.php?file=../../../../etc/flag.txt

On a un une longue chaine encodée en base64. Encodage qui nous est très familier ;)

On décode la chaine via https://www.base64decode.org/

On obtient une nouvelle chaine en base 64 précédée de `data:image/png;base64,`

Là on comprend que c'est une image png qui est encodée en base64

Il ne nous reste qu'à passer du code en base64 à l'image PNG

On se sert de https://base64.guru/converter/decode/image/png

On obtient facilement l'image finale qui nous révèle le flag

![Alt text](https://bitbucket.org/ben_tby/hackerlab2019-write-up-403-forbidden/downloads/base64PNG.png "Flag trouvé")

Le Flag est donc `CTF_AVOIDLFIWHILEPROGRAMMING`