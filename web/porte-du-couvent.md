## Challenge: La porte du couvent | 400pts

> *Dans leur élan pour secourir les dernières victimes du célèbre Gayeman DJAKPATAGLO, les cyber-amazones font face à la porte d'un couvent impénétrable. http://hackerlab.bj:8001/web03/web03_frjofjzefzf.html*

En suivant le lien fourni on a une page de connexion

![Alt text](https://bitbucket.org/ben_tby/hackerlab2019-write-up-403-forbidden/downloads/login.png "login")

Après une tentative de connexion infructueuse on obtient une chaine codée en base64

On la déchiffre et on obtient **vous n'êtes plus loin du login: e7791ece9da5a6e3f0aa6ba77536fbc6b7780895**

La valeur donnée est un hash. Découvrons de quel type de hash il s'agit.

Grâce à l'utilitaire `hash-identifier` on découvre que c'est un hash SHA-1

On le décrypte avec https://www.dcode.fr/hash-sha1

Le résultat est `amazone`

On connait à présent le login.
Il ne manque que le password. Après maintes tentatives infructueuses on décide de consulter le `hint` qui dit ceci

> type juggling + md5

On sait désormais quelle méthode utiliser pour bypasser le password

Une liste des payloads utilisables pour le type juggling est disponible à cette adresse https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/Type%20Juggling

Dans notre cas le payload à utiliser est `240610708`
On remplit donc le formulaire comme suit
`login:amazone password:240610708`

On obtient le flag en texte clair

Le Flag est donc `Flag: CTF_NOMDEHACKERCEQUETUESFORT`