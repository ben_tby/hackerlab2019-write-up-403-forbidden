# Challenge Armageddon (End Game) | 300

>    Votre unité de cyber-amazones est envoyée pour défendre le pays contre l'Armageddon invoqué par DJAKPATAGLO. C'est le clap de fin du #Hackerlab2019.

>    http://qualif.hackerlab.bj:8082


Un des challenges les plus drôles à résoudre pour notre équipe au Hackerlab.
On a contacté l'adresse web.

Nous avons constaté qu'il resiste aux ` attaques XSS`et à des commandes `php` et `sql` que nous avons éssayées.

Nous avons checké le **robots.txt**, ce qui nous a permis de lire `/README.txt` et `/web.config`, toujours rien de concluant. Par intuition, nous avons essayé ``http://qualif.hackerlab.bj:8082/flag.txt``.

Idée de génie puisque finalement le flag s'y trouve. 


# Flag: CTF_CyberAmazonWinArmagedon