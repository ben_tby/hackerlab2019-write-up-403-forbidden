## Challenge: Une mémoire intrigante (1/4) | 200pts

> *Le bjCSIRT a été dépeché pour assiter l'OCRC lors de la perquisition du domicile du bras droit de DJAKPATAGLO. Arrivé sur les lieux, ils ont fait l'inventaire des objets trouvés. Parmi ces objets, un ordinateur allumé. L'équipe a naturellement procédé à l'acquisition de la mémoire vive de cet ordinateur. Lors de la phase d'analyse, ils ont découvert une connexion étrange. Suivez la !*

Ce challenge parle d'une connexion étrange à retrouver. à partir de là on analyse le fichier de mémoire afin d'y trouver des données de connexion. Pour ce faire on combinera les utilitaires `strings` et `grep`

On dresse une liste de chaines de caractères à rechercher dans le fichier de mémoire
Étant donné qu'on cherche une connexion les chaines choisies étaient entre autres "ftp", "telnet", "ssh"
La  syntaxe à utiliser était `strings memdump.mem | grep -n chaine_a_rechercher`

La recherche sur la chaine "telnet" nous donne une piste à explorer
Comme le montre la capture ci dessous, à la ligne 9.849.880 du fichier de mémoire il y a eu une connexion telnet


![Alt text](https://bitbucket.org/ben_tby/hackerlab2019-write-up-403-forbidden/downloads/telnet01.png "connexion trouvée")

On décide de suive cette connexion en exécutant `telnet  51.83.76.195 42000`
 et on obtient en réponse le flag 