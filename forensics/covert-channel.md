## Challenge: Covert Channel | 200pts

> *Le gayeman DJAKPAGLO est maintenant aux mains de l'OCRC. Mais Il semble que DJAKPATAGLO ait des espions au sein du bjCSIRT. Ces derniers arrivent à contourner les mesures de sécurité mises en place pour exfiltrer des données vers leur chef en prison. Le SOC du bjCSIRT a répéré cela à travers une capture réseau, qu'il vous envoie pour analyse.*

Là on a un fichier compressé contenant un fichier de capture

On ouvre le fichier de capture pour l'inspecter

On relève queslques anomalies dans les communications

+ Un grand nombre de réponses http `404 Not Foud`
+ Beaucoup de demandes de résolutions DNS aux contenus très suspects
* Aucune résolution DNS réussie

D'office on se dit qu'il y'a anguille sous roche. Bon peut-être pas anguille mais y'a tout de même quelque chose sous roche.
Focus sur le *contenu suspect* de ces requêtes DNS
On voit qu'à chaque fois y'a une suite de caractères hexadécimaux en début de l'adresse soumise en requête tandis que la suite de l'adresse `.cyber.hackirlab.local` reste identique dans toutes les requêtes. On pense que ce sont des hash... Mauvaise piste
On oublie l'hypothèse des hash. On revient dans les possibilités les plus basiques... Et si c'était juste du texte codé en hexadécimal ?
Eh oui... C'est bel et bien du texte codé en HEX.
On concatène toutes les chaines HEX contenus dans les requêtes DNS pour pouvoir déchiffrer le tout

Le résultat des concaténations donne:

`4c6f72656d20697073756d20646f6c6f722073697420616d65742c20636f6e73656374657475722061646970697363696e6720656c69742c2073656420646f20656975736d6f642074656d706f7220696e6369646964756e74207574206c61626f726520657420646f6c6f7265206d61676e6120616c697175612e20557420656e696d206164206d696e696d2076656e69616d2c2071756973206e6f737472756420657865726369746174696f6e20756c6c616d636f206c61626f726973206e69736920757420616c697175697020657820656120636f6d6d6f646f20636f6e7365717561742e204354465f466f7273656e7369634275737465722044756973206175746520697275726520646f6c6f7220696e20726570726568656e646572697420696e20766f6c7570746174652076656c697420657373652063696c6c756d20646f6c6f726520657520667567696174206e756c6c612070617269617475722e204578636570746575722073696e74206f6363616563617420637570696461746174206e6f6e2070726f6964656e742c2073756e7420696e2063756c706120717569206f666669636961206465736572756e74206d6f6c6c697420616e696d20696420657374206c61626f72756d0a`


On déchiffre avec https://codebeautify.org/hex-string-converter et on obbtient le texte clair 


![Alt text](https://bitbucket.org/ben_tby/hackerlab2019-write-up-403-forbidden/downloads/cleartext.png "Clear Text")

Le Flag est donc `CTF_ForsensicBuster`