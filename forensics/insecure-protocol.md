## Challenge: Insecure protocol | 150pts

> <Pas d'énoncé... Juste un fichier de capture pcap>

Là on a un fichier de capture à inspecter

Pour commencer on remarque des communications `http`

On se dit qu'on a trouvé le *Insecure protocol*

Alors va t'on se mettre à  fouiller packet par packet ??
Absolument non !!

On va tout simplement, avec notre `Wireshark`, aller dans `Fichier` > `Exporter Objets` > `HTTP...` 

On obtient une liste d'objets http

![Alt text](https://bitbucket.org/ben_tby/hackerlab2019-write-up-403-forbidden/downloads/export.png "Http Objects")

On les exporte tous dans un répertoire pour les analyser

![Alt text](https://bitbucket.org/ben_tby/hackerlab2019-write-up-403-forbidden/downloads/exported.png "Exported Objects")

On remarque un fichier qui revient 6 fois. Il s'agit de `action_page.php` De quoi attirer l'attention des forensic inverstigators de `403 | F0RB!DD3N`

On affiche donc son contenu qui tient en une ligne `uname=admin&psw=CTF_forensicinvestigator&remember=on`

Le Flag est donc `CTF_forensicinvestigator`