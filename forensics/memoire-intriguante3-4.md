## Challenge: Une mémoire intrigante (3/4) | 200pts

> *Le bjCSIRT a été dépeché pour assiter l'OCRC lors de la perquisition du domicile du bras droit de DJAKPATAGLO. Arrivé sur les lieux, ils ont fait l'inventaire des objets trouvés. Parmi ces objets, un ordinateur allumé. L'équipe a naturellement procédé à l'acquisition de la mémoire vive de ce ordinateur. Les sites visités par le suspects ont naturellement intéressé l'équipe d'investigation.*

Ce challenge demande qu'on s'intéresse aux sites consultés par le suspect.

Premier réflexe: extraire toutes les adresses web  consultées par DJAKPATAGLO dans le fichier de mémoire

On exporte cette liste d'adresses dans un fichier nommé `web.txt` pour parcourir avec plus de facilité

On procède à l'exportation avec la commande `strings memdump.mem | grep http >> web.txt`

> Il est à noter que cette commande nous donnera en sortie à la fois les lignes contenant "http" et celles contenant "https"

Pendant qu'on parcoure le fichier obtenu, une adresse attire l'attention. Il s'agit de `http://guidos.hostingerapp.com/fnroungforzg.txt`

On la suit et bingo! Le flag s'y trouve